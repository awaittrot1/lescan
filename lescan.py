from pydbus import SystemBus
from pydbus import Variant
from gi.repository import GLib
import re

class BleLogger:
    """ble logger class"""

    def __init__(self):
        self.data = dict()
        self.rssi_max = -120
        self.rssi_min = 0

    def update(self, addr, val):
        if not addr in self.data:
            self.data[addr] = dict()

        if val:
            self.data[addr].update(val)
        else:
            return

        if "RSSI" in val:
            if val["RSSI"] < self.rssi_min:
                self.rssi_min = val["RSSI"]
            if val["RSSI"] > self.rssi_max:
                self.rssi_max = val["RSSI"]

    def show(self):
        print("min: {}, max: {}".format(self.rssi_min, self.rssi_max))
        print("{:20s}{:45s}".format("address", "rssi"))
        for k, v in self.data.items():
            d = (self.rssi_max - self.rssi_min ) / 40

            if d == 0:
                d = 1

            s = "*"
            if "RSSI" in v:
                for i in range(round((v["RSSI"] - self.rssi_min)/ d)):
                    s += "*"
                print("{:20s}{:5}{:40s}".format(k, v["RSSI"], s))

bus = SystemBus()
logger = BleLogger()

def ble_update(dev, prop):
    logger.update(dev, prop)
    logger.show()

def interfaces_added(path, prop):
    dev = bus.get("org.bluez", path)
    dev.PropertiesChanged.connect(lambda a,b,c: ble_update(path, b))

loop = GLib.MainLoop()
dev = bus.get("org.bluez", "/org/bluez/hci0")
api = dev["org.bluez.Adapter1"]

root = bus.get("org.bluez", "/")
for k in root.GetManagedObjects():
    if re.match("/org/bluez/hci0/dev_.[0-9_]+", k):
        print(k)
        interfaces_added(k, None)

api.SetDiscoveryFilter({ "Transport": Variant("s", "le"), "RSSI": Variant("n", -90) })
api.StartDiscovery()
root.InterfacesAdded.connect(interfaces_added)

GLib.MainLoop().run()
